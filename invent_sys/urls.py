"""invent_sys URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Import the include() function: from django.conf.urls import url, include
    3. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import url
from django.contrib import admin
from signup.views import *
from django.contrib.auth.views import login,logout


admin.autodiscover()
urlpatterns = [
    url(r'^dashboard/',inventory_dashboard.as_view(), name ='dashboard'),
    url(r'^inventory_detail/(?P<id>[0-9]+)$',inventory_details.as_view(),name="inven_detail_pg"),
    url(r'^signup/', sign_form_view.as_view(),name='sign_pg'),
    url(r'^login/',login, {'template_name':'login_fo.html'}, name ='login_pg'),
    url(r'^logout/$', logout, {'next_page':'dashboard'}, name ='logout_pg'),
    url(r'^inve_create/$',create_inventory.as_view(), name='new_inventory'),
    url(r'^item_create/$',create_item.as_view(), name='new_item'),
    url(r'^profile_up/',profile_update.as_view(), name='profile_update'),
    url(r'^provision/$',provision_item.as_view(),name='provision_item'),
    url(r'^profile/',profile_show.as_view(),name='profile_show'),
    url(r'^change_password/',change_password.as_view(),name='change_password'),
    # url(r'^delete/$',name='delete'),
    url(r'^admin/', admin.site.urls),
]

