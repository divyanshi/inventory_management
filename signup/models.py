from __future__ import unicode_literals
from django.db import models
import datetime
from datetime import timedelta
from django.conf import settings
from django.core.validators import RegexValidator
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)


class Cus_User_manager(BaseUserManager):

    def create_user(self, **kwargs):

        if not kwargs.get('email_add'):
            raise ValueError("User must have an email address")
        password = kwargs.pop('password')
        user_ob = self.model(**kwargs)

        user_ob.is_active = True
        user_ob.is_admin = False
        user_ob.set_password(password)
        user_ob.save(using=self._db)
        return user_ob

    def create_superuser(self,**kwargs):

        admin_ob = self.create_user(**kwargs)
        admin_ob.is_active = True
        admin_ob.is_admin = True
        # admin_ob.set_password()
        admin_ob.save(using = self._db)
        return admin_ob


class Cus_User(AbstractBaseUser):
    #make email id and gender non editable
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email_add = models.EmailField(unique=True,help_text="Case Sensitive")
    DOB = models.DateField()
    profilepic = models.ImageField(blank=True,null=True,upload_to="signup/pictures")
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    phone= models.CharField(validators=[phone_regex], max_length=15,blank=True)
    address = models.TextField()
    GEN = (
        ("M", "Male"),
        ("F", "Female"),
    )
    gender = models.CharField(max_length=1,choices=GEN)

    objects = Cus_User_manager()

    USERNAME_FIELD = 'email_add'

    REQUIRED_FIELDS = [first_name,last_name,email_add,DOB,phone,address,gender]

    def get_short_name(self):
        return self.first_name

    def get_full_name(self):
        return self.email_add

    def __str__(self):
        return self.email_add

class inventory(models.Model):
    #make name non editable
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='invent_owner')
    name = models.CharField(max_length=10,unique=True)
    description = models.TextField(max_length=500)
    creat_date = models.DateTimeField(default=datetime.datetime.now())
    members = models.ManyToManyField(settings.AUTH_USER_MODEL,related_name='member_of')

    def __str__(self):
        return self.name


class item(models.Model):
    i_name = models.CharField(max_length=10)
    i_desc = models.TextField(max_length=20)
    returnable = models.BooleanField(default=False)
    quantity = models.IntegerField() #amount currently available in inventory
    invent = models.ForeignKey(inventory,related_name="belongs_to",on_delete=models.CASCADE)

    def __str__(self):
        return self.i_name


class item_member_map(models.Model):
    item_name = models.ForeignKey(item,on_delete=models.CASCADE,related_name='item_name')
    issued_to = models.ForeignKey(settings.AUTH_USER_MODEL,related_name='issued_user')
    quantity_issued = models.IntegerField(default=1)
    return_date = models.DateField(default = datetime.datetime.now()+timedelta(7),blank=True)
    returned_on = models.DateField(null=True)
