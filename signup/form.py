from . import models
import datetime
from datetime import timedelta

from django import forms


class signup_fo(forms.ModelForm):
    pswrd = forms.CharField(label='Password',widget=forms.PasswordInput)
    class Meta:
        model = models.Cus_User
        fields=['first_name','last_name','email_add', 'DOB','profilepic','phone','address','gender']


class inventory_fo(forms.ModelForm): # form for creating inventory

    def __init__(self,*args,**kwargs):
        request = kwargs.pop('request')
        super(inventory_fo, self).__init__(*args,**kwargs)
        self.fields['members'].queryset = models.Cus_User.objects.all().exclude(id = request.user.id)


    class Meta:
        model = models.inventory
        fields =['name', 'description','members']
        widgets ={'members': forms.CheckboxSelectMultiple()}



class item_fo(forms.ModelForm): # form for creating item
    class Meta:
        invent = forms.ChoiceField(label="Under Inventory",widget=forms.CheckboxInput())
        model = models.item
        fields =['i_name','i_desc','returnable','quantity','invent']


class item_member_map(forms.ModelForm):
    class Meta:
        model = models.item_member_map
        fields = ['item_name','issued_to','quantity_issued','return_date','returned_on' ]