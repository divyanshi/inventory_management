from django.shortcuts import render,get_object_or_404
from django.core.mail import send_mail
from django.http import HttpResponseRedirect
# import datetime
# from datetime import timedelta
from django.http import HttpResponse
from .models import *
from django.views.generic import View,CreateView,UpdateView,DetailView,DeleteView
from .form import *
from invent_sys.settings import EMAIL_HOST_USER
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse_lazy
from invent_sys import settings
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm


class sign_form_view(View):
    def get(self,request):
        form = signup_fo()
        return render(request,'signup_fo.html', {'form':form})

    def post(self,request):
        form = signup_fo(request.POST)
        if form.is_valid():
            to_list = []
            to_list.append(form.cleaned_data.get('email_add'))
            if send_mail(subject='Email Confirmation',message='Hello! Welcome '+form.cleaned_data.get('first_name'),
                         from_email=EMAIL_HOST_USER,
                         recipient_list = to_list) == 1:
                user_data = Cus_User.objects.create_user(**form.cleaned_data)

                #user_data.save() #if email address exists save the data
                #confirm whether we have to use User object and also sending a link and confirming through that
        return render(request,'signup_fo.html',{'form':form})


settings.LOGIN_REDIRECT_URL='dashboard'


@method_decorator(login_required,name='dispatch') # name of method/dispatch
class inventory_dashboard(View):
    inven_own_list = inventory.objects.all().order_by('creat_date')
    inven_member_list = inventory.objects.all().order_by('creat_date')

    def get(self,request):
        inven_own_list = self.inven_own_list.filter(owner=request.user)
        inven_member_list = self.inven_member_list.filter(members=request.user)
        context_dict = {}
        context_dict['owner_of'] = inven_own_list
        context_dict['member_of'] = inven_member_list
        context_dict['data']= "X"
        return render(request,'table.html',context_dict)

    def post(self,request):
        inven_own_list = self.inven_own_list.filter(owner=request.user)
        inven_member_list = self.inven_member_list.filter(members=request.user)
        print request.POST.get('filter_data')
        if request.POST.get('filter_data') == "member":
            inven_own_list = []
        elif request.POST.get('filter_data') == "owner":
            inven_member_list = []
        context_dict = {}
        context_dict['owner_of'] = inven_own_list
        context_dict['member_of'] = inven_member_list
        context_dict['data'] = request.POST.get('filter_data') or "X"
        return render(request, 'table.html', context_dict)

settings.LOGIN_REDIRECT_URL='new_inventory'
@method_decorator(login_required,name='dispatch')
class create_inventory(View):

    def get(self,request):
        inventory_form = inventory_fo(**{'request':request})
        print inventory_form
        return render(request, 'inven_fo.html', {'inven_form': inventory_form})

    def post(self,request):
        inventory_form = inventory_fo(request.POST,**{'request':request})
        if inventory_form.is_valid():
            members = inventory_form.cleaned_data.pop('members')
            temp_inv=inventory_form.save(commit=False)
            temp_inv.owner = request.user
            temp_inv.creat_date = datetime.datetime.now()
            temp_inv.save()
            for member in members:
                temp_inv.members.add(member)
            temp_inv.save()

        else:
            return HttpResponse("Invalid form data")

        return render(request,'inven_fo.html',{'inven_form':inventory_form})


# settings.LOGIN_REDIRECT_URL=''
@method_decorator(login_required,name='dispatch')
class inventory_details(View):

    def get(self,request,*args,**kwargs):
        inv_id = kwargs.get('id')
        invent = inventory.objects.filter(id = inv_id)
        if request.user == invent.owner:
            owner = True
            item_obj_list = item.object.filter(invent=invent)
        else:
            items = item.objects.filter(invent=invent)
            for itr in items:
                item_provisioned = item_member_map.objects.filter(item_name=itr,issued_to=request.user)
            owner = False


        return HttpResponse('Congrats!')

        #return render(request,'inventory_detail_page.html',{'owner':inven_owner})


# settings.LOGIN_REDIRECT_URL=''
@method_decorator(login_required,name='dispatch')
class create_item(View):
    def get(self,request):
        form = item_fo()
        return render(request,'item_fo.html', {'form': form})

    def post(self,request):
        form = item_fo(request.POST)
        if form.is_valid():
            item_s = item(**form.cleaned_data)
            if not item_s.return_date:
                item_s.return_date = datetime.datetime.now()+timedelta(7)
            item_s.save()
        else:
            return HttpResponse("Invalid form data")

        sub = "New Item in Inventory " + item_s.invent
        msg = "Hello, "+ request.user.first_name + "! New Item is created: "+item_s.i_name

        inv_belong = inventory.objects.filter(id = item_s.invent.id)
        to_list =[]
        for itr in inv_belong.members:
            to_list.append(itr)
        to_list.append(EMAIL_HOST_USER)
        send_mail(subject=sub,message=msg,from_email=EMAIL_HOST_USER,recipient_list=to_list)
        return render(request, 'item_fo.html', {'form': form})


# settings.LOGIN_REDIRECT_URL=''
@method_decorator(login_required,name='dispatch')
class provision_item(CreateView):
    model = item_member_map
    fields = ['item_name','issued_to','quantity_issued','return_date' ]
    template_name = "provision_item_form.html"
    success_url = reverse_lazy('dashboard')

    def get_object(self):
        return get_object_or_404(item_member_map, pk=self.request.user.id)

# settings.LOGIN_REDIRECT_URL=''
@method_decorator(login_required, name='dispatch')
class profile_show(DetailView):

    template_name = 'profile.html'

    def get_object(self):
        return get_object_or_404(Cus_User, pk=self.request.user.id)

# settings.LOGIN_REDIRECT_URL=''
@method_decorator(login_required,name='dispatch')
class profile_update(UpdateView):
        template_name = 'user_update.html'
        fields =['first_name','last_name','DOB','profilepic','phone','address']
        success_url = reverse_lazy('dashboard')

        def get_object(self):
            return get_object_or_404(Cus_User,pk=self.request.user.id)


@method_decorator(login_required, name='dispatch')
class change_password(View):
    def get(self,request):
        form = PasswordChangeForm(request.user)
        return render(request,'change_pswrd.html',{'form':form})

    def post(self,request):
        messages.success(request, 'Your password is successfully changed ! ')
        form = PasswordChangeForm(request.user,request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request,user)
        return render(request,'change_pswrd.html',{'form':form})

# settings.LOGIN_REDIRECT_URL=''
@method_decorator(login_required,name='dispatch')
class inventory_view(UpdateView):
        template_name = 'inv_update_form.html'
        fields =['description','members']
        success_url = reverse_lazy('dashboard')

        def get_object(self):
            return get_object_or_404(inventory,pk=self.request.user.id)


# settings.LOGIN_REDIRECT_URL=''
@method_decorator(login_required,name='dispatch')
class item_view(UpdateView):
        template_name = 'item_upd_form.html'
        fields =['i_name','description','quantity','invent']
        success_url = reverse_lazy('inven_detail_pg')

        def get_object(self):
            return get_object_or_404(item,pk=self.request.user.id)


# settings.LOGIN_REDIRECT_URL=''
@method_decorator(login_required,name='dispatch')
class item_del_view(DeleteView):
        template_name = 'item_delete.html'
        success_url = reverse_lazy('inven_detail_pg')

        def get_object(self):
            return get_object_or_404(item,pk=self.request.user.id)