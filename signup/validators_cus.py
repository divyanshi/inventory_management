from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
import string


def validate_password(password):
    length=6
    if len(password)>=length:
        if password.alpha():
            if password.isdigit():
                special_char = set(string.punctuations)
                if any(char in special_char for char in password):
                    return True #check what validators should return
                else:
                    ValidationError(
                        _('%(pswrd)s should contain atleast 1 special characters'),params={'pswrd': password},
                    )
            else:
                ValidationError(
                    _('%(pswrd)s should contain atleast 1 digit'),params={'pswrd': password},
                )
        else:
            ValidationError(
                _('%(pswrd)s should contain atleast 1 Alphabhet'),params={'pswrd': password},
            )
    else:
        ValidationError(
            _('%(pswrd)s should contain atleast %(length) characters '),
            params={'pswrd': password ,'length':length},
        )

