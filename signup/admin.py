from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from signup.models import Cus_User
#from django import forms
from .form import signup_fo
from django.contrib.auth.models import Group

# class UserChangeForm(forms.ModelForm):
#     """A form for updating users. Includes all the fields on
#     the user, but replaces the password field with admin's
#     password hash display field.
#     """
#
#     class Meta:
#         model = Cus_User
#         fields = ('first_name','last_name','email_add','pswrd', 'DOB','profilepic','phone','address',
#                   'gender', 'is_active', 'is_admin')
#
#     def clean_password(self):
#         # Regardless of what the user provides, return the initial value.
#         # This is done here, rather than on the field, because the
#         # field does not have access to the initial value
#         return


class UserAdmin(BaseUserAdmin):

    add_form_template ='signup_fo.html'
    #form = UserChangeForm
    add_form = signup_fo

    list_display = ('first_name','last_name','email_add', 'DOB','profilepic','phone','address','gender')
    list_filter = ()
    fieldsets = (
        (None, {'fields': ('email_add', 'pswrd')}),
        ('Personal info', {'fields': ('first_name','last_name','DOB','profilepic','phone','address','gender')}),
    )
    filter_horizontal = ()
    ordering =['email_add']


admin.site.register(Cus_User, UserAdmin)
admin.site.unregister(Group)